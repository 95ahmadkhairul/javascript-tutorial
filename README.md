# DAFTAR ISI
1. Pengenalan dasar Pemrograman Javascript
2. Memahami 4 Cara Penulisan Javascript pada HTML
3. Cara Menampilkan Output di Javascript
4. Variabel dan Tipe Data dalam Javascript
5. Mengenal 3 Macam Jendela Dialog pada Javascript
6. Mengenal 6 Macam Operator pada Javascript
7. Memahami 6 Bentuk Blok Percabangan pada Javascript
8. Belajar 5 Macam Jenis Blok Perulangan pada Javascript
9. Mengenal Struktur Data Array pada Javascript
10. Mengenal DOM API untuk Manipulasi HTML
11. Memahami Fungsi pada Javascript
12. Mengenal Objek di Javascript
13. Mengenal Objek Math untuk Perhitungan Matematika
14. Belajar Menggunakan AJAX	
15. Javascript Regex

# DAFTAR PUSTAKA :
1. https://www.petanikode.com/tutorial/javascript/
2. https://www.w3schools.com/jsref/jsref_obj_regexp.asp